﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct database
{
    int an, durata;
    char titlu[100], categorie[100], premii[100];
} database;

FILE *fis;

void afisareDatabase(database db[], int index)
{
    for (int i = 0; i <= index; i++)
    {
        printf("%d %d %s %s %s", db[i].an, db[i].durata, db[i].titlu, db[i].categorie, db[i].premii);
    }
}

void sortareSimplaDupaTitluAlfabetic(database db[], int index)
{
    database aux;
    for (int i = 0; i < index - 1; i++)
        for (int j = i + 1; j < index; j++)
        {
            if (strcmp(db[j].titlu, db[j + 1].titlu) < 0)
            {
                aux = db[i];
                db[i] = db[j];
                db[j] = aux;
            }
        }
}
//Prima cerinta
// S2. Sa se afiseze filmele in functie de durata, in ordine crescatoare, folosind sortarea prin insertie
void sortarePrinInsertieDupaDurata(database db[], int index){
    int i,j;
    database tmp;
    for(i=1;i<index;i++){
        tmp=db[i];
        for(j=i; (j>0) && (tmp.durata > db[j-1].durata);j--){
            db[j]=db[j-1];
        }
        db[j]=tmp;
    }

//A 2 a cerinta 
// S2'. Sa se ordoneze printr-o singură parcurgere a tabloului filmele astfel încât filmele din categorie "Action" să fie pe
// primele poziții urmate de celelalte filme, fără a folosi o altă structuraă de tip tablou, față de cea inițială.
}
void sortarePrinInsertieDupaCategorie(database db[], int index){
    int i,j;
    database tmp;
    for(i=1;i<index;i++){
        tmp=db[i];
        for(j=i; (j>0) && (strcmp(tmp.categorie,"Action") == 0 && strcmp(db[j-1].categorie, "Action") !=0);j--){
            db[j]=db[j-1];
        }
        db[j]=tmp;
    }
}
int cautare_binara(database a[], int n, int x)
{
	int stanga, dreapta, mij;
	int gasit;
	stanga = 0; dreapta = n - 1; gasit = 0;
	while ((stanga <= dreapta) && (!gasit))
	{
		/* aici este modificarea*/
		mij = (stanga + dreapta) / 2;
		if (a[mij].durata<x) stanga = mij + 1;
		else if (a[mij].durata>x) dreapta = mij - 1;
		else if (a[mij].durata == x) gasit = 1;
	}
	if (gasit) /*avem o coincidenţă la indexul i*/
		return mij;
	else return -1;
}
int main()
{
    database *db = NULL;
    char string[200];
    char *str;
    int contor = 0;
    int index2;
    if ((fis = fopen("filme.txt", "rt")) == NULL)
    {
        printf("Eroare la deschiderea fisierului.");
        exit(EXIT_FAILURE);
    };

    int index = 0;
    while (fgets(string, 100, fis))
    {
        str = strtok(string, "\t");

        db = (database *)realloc(db, (index + 1) * sizeof(database));
        contor = 0;
        while (str != NULL)
        {

            if (contor == 0)
            {
                db[index].an = atoi(str);
            }
            else if (contor == 1)
            {
                db[index].durata = atoi(str);
            }
            else if (contor == 2)
            {
                strcpy(db[index].titlu, str);
            }
            else if (contor == 3)
            {
                strcpy(db[index].categorie, str);
            }
            else if (contor == 4)
            {
                strcpy(db[index].premii, str);
            }

            str = strtok(NULL, "\t");
            contor++;
        }
        index++;
    }
    

    // afisareDatabase(db, index);
    // sortareSimplaDupaTitluAlfabetic(db, index);
    // sortarePrinInsertieDupaDurata(db, index);
    //sortarePrinInsertieDupaCategorie(db, index);
   // afisareDatabase(db, index);
    index2 = cautare_binara(db, index, 147 );
    printf("%d %d %s %s", db[index2].an, db[index2].durata, db[index2].titlu, db[index2].premii);
    fclose(fis);
    free(db);
    return 0;
}
