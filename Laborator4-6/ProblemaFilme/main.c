// I. Să se realizeze o aplicație pentru gestionarea unor filme, a căror informații sunt reținute în fișierul Filme.txt. Formatul fișierului este următorul:

// An    Durata    Titlu    Categorie    Premii

// An- intreg, Durata - intreg, Titlu - sir de caractere continand spatii, Categorii - sir de caractere, Premii (Da/Nu).

// Se consideră ca separator între elemente TAB ('\t')

// Să se implementeze următoarele funcționalități:

// S1. Să se citească datele din fișier și să se definească structurile de date potrivite

// S2. Să se afișeze filmele alfabetic după titlu folosind o sortare simplă, in situ.

// S2'. Să se ordoneze printr-o singură parcurgere a tabloului filmele astfel încăt filmele premiate să fie pe primele poziții urmate de ftă structură de tip tablou, față de cilmele nepremiate, fără a folosi o alea inițială.

// S3.  Să se afișeze filmele crescător după anul apariției folosind quicksort sau o altă sortare avansată

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct database
{
    int an, durata;
    char titlu[100], categorie[100], premii[100];
} database;

FILE *fis;

void afisareDatabase(database db[], int index)
{
    for (int i = 0; i <= index; i++)
    {
        printf("%d %d %s %s %s", db[i].an, db[i].durata, db[i].titlu, db[i].categorie, db[i].premii);
    }
}

void sortareSimplaDupaTitluAlfabetic(database db[], int index)
{
    database aux;
    for (int i = 0; i < index - 1; i++)
        for (int j = i + 1; j < index; j++)
        {
            if (strcmp(db[j].titlu, db[j + 1].titlu) < 0)
            {
                aux = db[i];
                db[i] = db[j];
                db[j] = aux;
            }
        }
}

int main()
{
    database *db = NULL;
    char string[200];
    char *str;
    int contor = 0;

    if ((fis = fopen("Filme.txt", "rt")) == NULL)
    {
        printf("Eroare la deschiderea fisierului.");
        exit(EXIT_FAILURE);
    };

    int index = 0;
    while (fgets(string, 100, fis))
    {
        str = strtok(string, "\t");

        db = (database *)realloc(db, (index + 1) * sizeof(database));
        contor = 0;
        while (str != NULL)
        {

            if (contor == 0)
            {
                db[index].an = atoi(str);
            }
            else if (contor == 1)
            {
                db[index].durata = atoi(str);
            }
            else if (contor == 2)
            {
                strcpy(db[index].titlu, str);
            }
            else if (contor == 3)
            {
                strcpy(db[index].categorie, str);
            }
            else if (contor == 4)
            {
                strcpy(db[index].premii, str);
            }

            str = strtok(NULL, "\t");
            contor++;
        }
        index++;
    }

    // afisareDatabase(db, index);
    sortareSimplaDupaTitluAlfabetic(db, index);
    afisareDatabase(db, index);
    fclose(fis);
    free(db);
    return 0;
}