#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct tip_nod
{
    char titlu_carte[100];
    char autor_carte[100];
    char nume_cititor[100];
    char adresa_cititor[100];
    int termen_returnare;
    struct tip_nod *urm;
} tip_nod;

typedef tip_nod tip_lista;

tip_lista *lista;

void vidare()
{
    lista = (tip_lista *)malloc(sizeof(tip_lista));
    lista = NULL;
}

void afisare()
{
    tip_lista *l2 = lista;
    while (l2->urm != NULL)
    {
        printf("IMPRUMUT - %s - %s - %d zile ramase ", l2->nume_cititor, l2->titlu_carte, l2->termen_returnare);
        printf("\n");
        l2 = l2->urm;
    }
    printf("IMPRUMUT - %s - %s - %d zile ramase ", l2->nume_cititor, l2->titlu_carte, l2->termen_returnare);
    printf("\n");
}

void imprumut_nou(char *adresa, char *autor, char *nume, char *titlu, int termenZile)
{
    tip_nod *nod = (tip_nod *)malloc(sizeof(tip_nod));
    if (nod)
    {
        strcpy(nod->adresa_cititor, adresa);
        strcpy(nod->autor_carte, autor);
        strcpy(nod->nume_cititor, nume);
        strcpy(nod->titlu_carte, titlu);
        nod->termen_returnare = termenZile;
        nod->urm = NULL;

        if (lista == NULL)
        {
            lista = nod;
        }
        else
        {
            tip_lista *last = lista;
            int ok = 1;
            while (last->urm != NULL)
            {
                if (!strcmp(nume, last->nume_cititor))
                {
                    ok = 0;
                    break;
                }
                last = last->urm;
            }
            if (ok == 1)
            {
                tip_lista *l1 = lista;
                while (l1->urm != NULL)
                {
                    l1 = l1->urm;
                }
                l1->urm = nod;
            }
            else
            {
                printf("RESTRICTIONAT, utilizatorul mai are un imprumut valid");
            }
        }
    }
    else
    {
        printf("Eroare la alocarea memoriei.");
    }
}

void actualizare()
{
    tip_lista *l4 = lista;
    while (l4->urm != NULL)
    {
        l4->termen_returnare = l4->termen_returnare - 1;
    }
    l4->termen_returnare = l4->termen_returnare - 1;
}

void returnare(char *numeCititor)
{
    tip_lista *l5 = lista;
    tip_lista *prev = lista;
    while (l5->urm != NULL && strcmp(l5->nume_cititor, numeCititor))
    {
        prev = l5;
        l5 = l5->urm;
    }
    prev->urm = l5->urm;
}

int main(void)
{
    vidare();
    imprumut_nou("Timisoara", "M. Eminescu", "Paul", "Luceafarul", 10);
    imprumut_nou("Timisoara", "M. Eminescu", "LUIZA", "ASAD SD", 10);
    imprumut_nou("Timisoara", "M. Eminescu", "George", "ASAD Ssfdf", 10);
    imprumut_nou("Timisoara", "M. Eminescu", "ANA", "VD D", -2);
    // imprumut_nou("Timisoara", "M. Eminescu", "ANA", "VD D", 10);
    afisare();
    // actualizare();
    // afisare();
    return 0;
}