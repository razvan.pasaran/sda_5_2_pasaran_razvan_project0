#ifndef INT_LIST_H
#define INT_LIST_H

typedef struct Node{
    int data;
    struct Node *next;
}Node;

typedef struct IntList{
    Node *head;
    int size;

}IntList;


void insertL(IntList *list, int value);

void deleteL(IntList *list, int value);

int size(const IntList *list);

int isEmpty(const IntList *list);

void printList(const IntList *list);

void destroyList(IntList *list);

#endif