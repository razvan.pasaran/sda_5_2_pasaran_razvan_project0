// AN bisect

#include <stdio.h>

int anBisect(int an)
{
    if ((an % 4 == 0 && an % 100 != 0) || (an % 400 == 0))
        return 1;
    else
        return 0;
}

int Exponent(int baza, int exponent)
{
    int rez = 1;
    int i = 0;
    while (i < exponent)
    {
        rez = rez * baza;
        i++;
    }
    return rez;
}

int main(void)
{
    int an, baza, exponent;
    printf("an=");
    scanf("%d", &an);
    printf("baza=");
    scanf("%d", &baza);
    printf("exponent=");
    scanf("%d", &exponent);
    printf("%d\n", anBisect(an));
    printf("%d\n", Exponent(baza, exponent));
    return 0;
}