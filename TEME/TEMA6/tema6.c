// CURS 6 - - - E1. Să se implementeze o funcție de ștergere a unui nod cu o cheie dată dintr-o listă simplu
// înlănțuită. Funcția trebuie să ia în considerare toate cazurile de ștergere.

#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
} Node;

void adauga(Node **head_ref, int new_data)
{
    Node *new = (Node *)malloc(sizeof(Node));
    Node *last = *head_ref;
    if (new)
    {
        new->data = new_data;
        new->next = NULL;
    }
    if (*head_ref == NULL)
    {
        *head_ref = new;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new;
}

void afisare(Node *nod)
{
    while (nod != NULL)
    {
        printf("%d ", nod->data);
        nod = nod->next;
    }
    printf("\n");
}

void deleteNode(Node **head_ref, int key)
{
    Node *temp = *head_ref, *prev;
    // DACA NODUL CAUTAT ESTE PRINUL NOD
    if (temp != NULL && temp->data == key)
    {
        *head_ref = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->data != key)
    {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
    {
        printf("Nu s-a gasit cheia.\n");
        return;
    }

    prev->next = temp->next;
    free(temp);
}

int main(void)
{
    Node *head = NULL;
    adauga(&head, 1);
    adauga(&head, 2);
    adauga(&head, 3);
    adauga(&head, 4);
    adauga(&head, 4);
    adauga(&head, 4);
    afisare(head);
    deleteNode(&head, 1);
    afisare(head);
    return 0;
}