// Ex2: Modificați algoritmul pentru sortarea prin inserție astfel încât sortarea să se facă de la
// sfârșitul tabloului spre început. Ordinea va rămâne tot crescătoare.

// void insertion_sort(tip_element a[], int n)
// {
// 	int i, j;
// 	tip_element tmp;
// 	for (i = 1; i < n; i++)
// 	{
// 		tmp = a[i];
// 		for (j = i; (j > 0) && (tmp.cheie < a[j - 1].cheie); j--)
// 			a[j] = a[j - 1];
// 		a[j] = tmp;
// 	}
// }

#include <stdio.h>

void insertion_sort(int a[], int n)
{
    int i, j;
    int tmp;
    for (i = n - 1; i >= 0; i--)
    {
        tmp = a[i];
        for (j = i; (j < n - 1) && (tmp > a[j + 1]); j++)
            a[j] = a[j + 1];
        a[j] = tmp;
    }
}

int main()
{
    int a[100], n;
    printf("n=");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
        scanf("%d", &a[i]);
    insertion_sort(a, n);
    for (int i = 0; i < n; i++)
        printf("%d ", a[i]);
    printf("\n");
    return 0;
}