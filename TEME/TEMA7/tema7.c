// Ex2. Să se implementeze o funcție de ștergere a unui nod cu o cheie dată dintr-o listă dublu
// înlănțuită. Funcția trebuie să ia în considerare toate cazurile de ștergere.

#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int key;
    struct Node *prev;
    struct Node *next;
};

struct Node *createNode(int key)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    newNode->key = key;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

void appendNode(struct Node **head, int key)
{
    struct Node *newNode = createNode(key);
    if (*head == NULL)
    {
        *head = newNode;
    }
    else
    {
        struct Node *last = *head;
        while (last->next != NULL)
        {
            last = last->next;
        }
        last->next = newNode;
        newNode->prev = last;
    }
}

void deleteNode(struct Node **head, int key)
{
    struct Node *current = *head;

    while (current != NULL && current->key != key)
    {
        current = current->next;
    }
    if (current == NULL)
    {
        printf("Nodul cu cheia data nu a fost gasit.\n");
        return;
    }
    if (current->prev != NULL)
    {
        current->prev->next = current->next;
    }
    else
    {
        *head = current->next;
    }
    if (current->next != NULL)
    {
        current->next->prev = current->prev;
    }
    free(current);
}

void printList(struct Node *head)
{
    struct Node *current = head;
    while (current != NULL)
    {
        printf("%d ", current->key);
        current = current->next;
    }
    printf("\n");
}

int main()
{
    struct Node *head = NULL;
    appendNode(&head, 1);
    appendNode(&head, 2);
    appendNode(&head, 3);
    appendNode(&head, 4);
    deleteNode(&head, 4);
    printList(head);
    return 0;
}