// Rescrieți funcția factorial astfel încât să nu fie recursivă

#include <stdio.h>

void fact(int n)
{

    if (n == 0 || n == 1)
        printf("1");
    else
    {
        int s = 1;
        for (int i = 2; i <= n; i++)
        {
            s = s * i;
        }
        printf("%d", s);
    }
}

int main(void)
{
    int n;
    scanf("%d", &n);
    fact(n);
    return 0;
}