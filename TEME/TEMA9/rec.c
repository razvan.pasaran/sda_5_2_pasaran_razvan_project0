// Ex1. Scrieți o funcție recursivă care numără nodurile dintr‐o listă simplu înlănțuită

#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int data;
    struct Node *next;
} Node;

void adauga(Node **head_ref, int new_data)
{
    Node *new = (Node *)malloc(sizeof(Node));
    Node *last = *head_ref;
    if (new)
    {
        new->data = new_data;
        new->next = NULL;
    }
    if (*head_ref == NULL)
    {
        *head_ref = new;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new;
}

void afisare(Node *nod)
{
    while (nod != NULL)
    {
        printf("%d ", nod->data);
        nod = nod->next;
    }
    printf("\n");
}

int nrNoduri(Node *head)
{
    if (head == NULL)
        return 0;
    else
        return 1 + nrNoduri(head->next);
}

int main(void)
{
    Node *head = NULL;
    adauga(&head, 1);
    adauga(&head, 2);
    adauga(&head, 3);
    adauga(&head, 4);
    adauga(&head, 4);
    adauga(&head, 4);
    afisare(head);
    printf("%d\n", nrNoduri(head));
    return 0;
}