#include <stdio.h>
#include <stdlib.h>
#include "timer.h"

#define NR 140

FILE *fis;
FILE *fout1;
FILE *fout2;
FILE *afisareTimp;

int N1[NR], N2[NR];

void citire(int *tablou, int size)
{
    fis = fopen("numere.txt", "r");
    for (int i = 0; i < size; i++)
    {
        fscanf(fis, "%d", &tablou[i]);
    }
    fclose(fis);
}

void bubbleSort(int *N1, int nr)
{
    int i, j, aux;
    for (i = 0; i <= nr - 1; i++)
        for (j = i + 1; j < nr; j++)
        {
            if (N1[i] > N1[j])
            {
                aux = N1[i];
                N1[i] = N1[j];
                N1[j] = aux;
            }
        }
}
void shakersort(int *N1, int nr)
{
    int stanga = 0, dreapta = nr - 1, aux;
    int interschimbare = 1;

    while (interschimbare)
    {
        interschimbare = 0;

        for (int i = stanga; i < dreapta; i++)
        {
            if (N1[i] > N1[i + 1])
            {
                aux = N1[i];
                N1[i] = N1[i + 1];
                N1[i + 1] = aux;
                interschimbare = 1;
            }
        }
        dreapta--;

        for (int i = dreapta; i > stanga; i--)
        {
            if (N1[i] < N1[i - 1])
            {
                aux = N1[i];
                N1[i] = N1[i - 1];
                N1[i - 1] = aux;
                interschimbare = 1;
            }
        }
        stanga++;
    }
}
void afisare(FILE *fout, int *N1, int nr)
{
    for (int i = 0; i < nr; i++)
    {
        fprintf(fout, "%d\n", N1[i]);
    }
}

int main()
{
    float timp;
    fout1 = fopen("numereOut1.txt", "w");
    fout2 = fopen("numereOut2.txt", "w");
    afisareTimp = fopen("afisareTimp.txt", "a");

    citire(N1, NR);
    citire(N2, NR);

    // contorizare timp shakersort
    starton();
    shakersort(N1, NR);
    timp = startoff();
    fprintf(afisareTimp, "Shakersort %d %f\n", NR, timp);

    // contorizare timp bubblesort
    starton();
    bubbleSort(N2, NR);
    timp = startoff();
    fprintf(afisareTimp, "Bubblesort %d %f\n", NR, timp);

    // afisare in fisier -- EXTRA
    afisare(fout1, N1, NR);
    afisare(fout2, N2, NR);

    fclose(fout1);
    fclose(fout2);
    fclose(afisareTimp);
    return 0;
}
