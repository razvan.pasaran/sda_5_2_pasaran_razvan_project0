#include <stdlib.h>
#include <stdio.h>
#include "timer.h"

#define NR 58

FILE *fout;

long long fibbonaciRecursiv(int n)
{
    if (n == 1 || n == 2)
        return 1;
    else
        return fibbonaciRecursiv(n - 1) + fibbonaciRecursiv(n - 2);
}

long long fibbonaciNerecursiv(int n)
{

    int nr1 = 1, nr2 = 2;
    int sum = 0;
    if (n == 1 || n == 2)
        return 1;
    else if (n == 3)
        return 2;
    else
    {
        for (int i = 0; i < n / 2; i++)
        {
            sum = nr1 + nr2;
            nr1 = nr2;
            nr2 = sum;
        }
    }
    return sum;
}

int main()
{
    float timp;
    fout = fopen("afisateTimpi.txt", "a");
    starton();
    fibbonaciRecursiv(NR);
    timp = startoff();
    fprintf(fout, "FibbonaciRecursiv %d %f\n", NR, timp);
    starton();
    timp = startoff();
    fprintf(fout, "FibbonaciNeRecursiv %d %f\n", NR, timp);
    return 0;
}